-- MySQL dump 10.13  Distrib 8.0.12, for Win64 (x86_64)
--
-- Host: localhost    Database: heatmap
-- ------------------------------------------------------
-- Server version	8.0.12

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `auth_group`
--

DROP TABLE IF EXISTS `auth_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group`
--

LOCK TABLES `auth_group` WRITE;
/*!40000 ALTER TABLE `auth_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group_permissions`
--

DROP TABLE IF EXISTS `auth_group_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_group_permissions_group_id_permission_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  KEY `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` (`permission_id`),
  CONSTRAINT `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group_permissions`
--

LOCK TABLES `auth_group_permissions` WRITE;
/*!40000 ALTER TABLE `auth_group_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_permission`
--

DROP TABLE IF EXISTS `auth_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_permission_content_type_id_codename_01ab375a_uniq` (`content_type_id`,`codename`),
  CONSTRAINT `auth_permission_content_type_id_2f476e4b_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_permission`
--

LOCK TABLES `auth_permission` WRITE;
/*!40000 ALTER TABLE `auth_permission` DISABLE KEYS */;
INSERT INTO `auth_permission` VALUES (1,'Can add markers',1,'add_markers'),(2,'Can change markers',1,'change_markers'),(3,'Can delete markers',1,'delete_markers'),(4,'Can add log entry',2,'add_logentry'),(5,'Can change log entry',2,'change_logentry'),(6,'Can delete log entry',2,'delete_logentry'),(7,'Can add permission',3,'add_permission'),(8,'Can change permission',3,'change_permission'),(9,'Can delete permission',3,'delete_permission'),(10,'Can add group',4,'add_group'),(11,'Can change group',4,'change_group'),(12,'Can delete group',4,'delete_group'),(13,'Can add user',5,'add_user'),(14,'Can change user',5,'change_user'),(15,'Can delete user',5,'delete_user'),(16,'Can add content type',6,'add_contenttype'),(17,'Can change content type',6,'change_contenttype'),(18,'Can delete content type',6,'delete_contenttype'),(19,'Can add session',7,'add_session'),(20,'Can change session',7,'change_session'),(21,'Can delete session',7,'delete_session'),(31,'Can view markers',1,'view_markers'),(34,'Can view log entry',2,'view_logentry'),(35,'Can view permission',3,'view_permission'),(36,'Can view group',4,'view_group'),(37,'Can view user',5,'view_user'),(38,'Can view content type',6,'view_contenttype'),(39,'Can view session',7,'view_session'),(40,'Can add hazard table',11,'add_hazardtable'),(41,'Can change hazard table',11,'change_hazardtable'),(42,'Can delete hazard table',11,'delete_hazardtable'),(43,'Can view hazard table',11,'view_hazardtable'),(44,'Can add injury table',12,'add_injurytable'),(45,'Can change injury table',12,'change_injurytable'),(46,'Can delete injury table',12,'delete_injurytable'),(47,'Can view injury table',12,'view_injurytable'),(48,'Can add img table',13,'add_imgtable'),(49,'Can change img table',13,'change_imgtable'),(50,'Can delete img table',13,'delete_imgtable');
/*!40000 ALTER TABLE `auth_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user`
--

DROP TABLE IF EXISTS `auth_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(128) NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(150) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(150) NOT NULL,
  `email` varchar(254) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user`
--

LOCK TABLES `auth_user` WRITE;
/*!40000 ALTER TABLE `auth_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_groups`
--

DROP TABLE IF EXISTS `auth_user_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_groups_user_id_group_id_94350c0c_uniq` (`user_id`,`group_id`),
  KEY `auth_user_groups_group_id_97559544_fk_auth_group_id` (`group_id`),
  CONSTRAINT `auth_user_groups_group_id_97559544_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `auth_user_groups_user_id_6a12ed8b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_groups`
--

LOCK TABLES `auth_user_groups` WRITE;
/*!40000 ALTER TABLE `auth_user_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_user_permissions`
--

DROP TABLE IF EXISTS `auth_user_user_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_user_permissions_user_id_permission_id_14a6b632_uniq` (`user_id`,`permission_id`),
  KEY `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` (`permission_id`),
  CONSTRAINT `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_user_permissions`
--

LOCK TABLES `auth_user_user_permissions` WRITE;
/*!40000 ALTER TABLE `auth_user_user_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_user_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_admin_log`
--

DROP TABLE IF EXISTS `django_admin_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin_log_content_type_id_c4bce8eb_fk_django_co` (`content_type_id`),
  KEY `django_admin_log_user_id_c564eba6_fk` (`user_id`),
  CONSTRAINT `django_admin_log_content_type_id_c4bce8eb_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  CONSTRAINT `django_admin_log_user_id_c564eba6_fk` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_admin_log`
--

LOCK TABLES `django_admin_log` WRITE;
/*!40000 ALTER TABLE `django_admin_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `django_admin_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_content_type`
--

DROP TABLE IF EXISTS `django_content_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `django_content_type_app_label_model_76bd3d3b_uniq` (`app_label`,`model`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_content_type`
--

LOCK TABLES `django_content_type` WRITE;
/*!40000 ALTER TABLE `django_content_type` DISABLE KEYS */;
INSERT INTO `django_content_type` VALUES (2,'admin','logentry'),(4,'auth','group'),(3,'auth','permission'),(5,'auth','user'),(6,'contenttypes','contenttype'),(11,'qutheatmap','hazardtable'),(13,'qutheatmap','imgtable'),(12,'qutheatmap','injurytable'),(1,'qutheatmap','markers'),(7,'sessions','session');
/*!40000 ALTER TABLE `django_content_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_migrations`
--

DROP TABLE IF EXISTS `django_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_migrations`
--

LOCK TABLES `django_migrations` WRITE;
/*!40000 ALTER TABLE `django_migrations` DISABLE KEYS */;
INSERT INTO `django_migrations` VALUES (1,'contenttypes','0001_initial','2018-04-27 00:13:04.267156'),(2,'auth','0001_initial','2018-04-27 00:13:05.400705'),(3,'admin','0001_initial','2018-04-27 00:13:05.689975'),(4,'admin','0002_logentry_remove_auto_add','2018-04-27 00:13:05.712033'),(5,'contenttypes','0002_remove_content_type_name','2018-04-27 00:13:05.916577'),(6,'auth','0002_alter_permission_name_max_length','2018-04-27 00:13:06.010828'),(7,'auth','0003_alter_user_email_max_length','2018-04-27 00:13:06.038401'),(8,'auth','0004_alter_user_username_opts','2018-04-27 00:13:06.048929'),(9,'auth','0005_alter_user_last_login_null','2018-04-27 00:13:06.145185'),(10,'auth','0006_require_contenttypes_0002','2018-04-27 00:13:06.150700'),(11,'auth','0007_alter_validators_add_error_messages','2018-04-27 00:13:06.161730'),(12,'auth','0008_alter_user_username_max_length','2018-04-27 00:13:06.407884'),(13,'auth','0009_alter_user_last_name_max_length','2018-04-27 00:13:06.519180'),(14,'qutheatmap','0001_initial','2018-04-27 00:13:06.567308'),(15,'sessions','0001_initial','2018-04-27 00:13:06.633985'),(16,'qutheatmap','0002_hazardinside','2018-05-07 23:21:33.488133'),(17,'qutheatmap','0003_auto_20180508_0937','2018-05-07 23:43:06.173266'),(18,'qutheatmap','0004_delete_hazardinside','2018-05-07 23:43:06.188896'),(19,'qutheatmap','0005_hazardinside','2018-05-07 23:43:06.188896'),(20,'qutheatmap','0006_delete_hazardinside','2018-05-07 23:43:52.570714'),(21,'qutheatmap','0007_hazardinside','2018-05-07 23:44:18.213250'),(22,'qutheatmap','0008_hazardoutside','2018-05-10 00:36:38.944116'),(23,'qutheatmap','0009_delete_hazardoutside','2018-05-10 00:38:03.402595'),(24,'qutheatmap','0010_injuryinside','2018-05-10 01:05:42.305998'),(25,'admin','0003_logentry_add_action_flag_choices','2018-08-26 01:35:30.525832'),(26,'qutheatmap','0011_hazardtable_injurytable','2018-08-26 01:35:45.658164'),(27,'qutheatmap','0012_auto_20180826_1229','2018-08-26 02:29:52.911417'),(28,'qutheatmap','0013_imgtable','2018-09-03 03:07:11.100806');
/*!40000 ALTER TABLE `django_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_session`
--

DROP TABLE IF EXISTS `django_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime(6) NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_expire_date_a5c62663` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_session`
--

LOCK TABLES `django_session` WRITE;
/*!40000 ALTER TABLE `django_session` DISABLE KEYS */;
/*!40000 ALTER TABLE `django_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `qutheatmap_aeds`
--

DROP TABLE IF EXISTS `qutheatmap_aeds`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `qutheatmap_aeds` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(250) NOT NULL,
  `lat` double NOT NULL,
  `lng` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qutheatmap_aeds`
--

LOCK TABLES `qutheatmap_aeds` WRITE;
/*!40000 ALTER TABLE `qutheatmap_aeds` DISABLE KEYS */;
INSERT INTO `qutheatmap_aeds` VALUES (1,'B Block',-27.476335,153.028575),(2,'R Block',-27.476918,153.027224),(3,'V Block',-27.477185,153.028383),(4,'O Block',-27.477654,153.028453),(5,'Z Block',-27.477918,153.02766),(6,'X Block',-27.477547,153.030021),(7,'P Block',-27.478011,153.028803);
/*!40000 ALTER TABLE `qutheatmap_aeds` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `qutheatmap_handicaplocation`
--

DROP TABLE IF EXISTS `qutheatmap_handicaplocation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `qutheatmap_handicaplocation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(250) NOT NULL,
  `lat` double NOT NULL,
  `lng` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qutheatmap_handicaplocation`
--

LOCK TABLES `qutheatmap_handicaplocation` WRITE;
/*!40000 ALTER TABLE `qutheatmap_handicaplocation` DISABLE KEYS */;
INSERT INTO `qutheatmap_handicaplocation` VALUES (1,'Z Block',-27.477719,153.027398),(2,'S Block',-27.477461,153.027229),(3,'C Block',-27.47843,153.028042),(4,'Eating Area',-27.478299,153.029169),(5,'C Block',-27.478655,153.02808),(6,'X Block',-27.477614,153.030347);
/*!40000 ALTER TABLE `qutheatmap_handicaplocation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `qutheatmap_hazardtable`
--

DROP TABLE IF EXISTS `qutheatmap_hazardtable`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `qutheatmap_hazardtable` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `description` varchar(250) NOT NULL,
  `incident_date` varchar(50) NOT NULL,
  `lat` double NOT NULL,
  `lng` double NOT NULL,
  `room_number` varchar(25) NOT NULL,
  `floor` varchar(10) NOT NULL,
  `weather` varchar(50) NOT NULL,
  `time` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=155 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qutheatmap_hazardtable`
--

LOCK TABLES `qutheatmap_hazardtable` WRITE;
/*!40000 ALTER TABLE `qutheatmap_hazardtable` DISABLE KEYS */;
INSERT INTO `qutheatmap_hazardtable` VALUES (1,'Slip Hazard','hazard1','2018-02-14',-27.476877,153.028413,'','','Overcast','5:05'),(2,'Slip Hazard','hazard2','2018-05-19',-27.476556,153.027557,'','','Clear','5:45'),(3,'Slip Hazard','hazard3','2018-01-24',-27.476371,153.027658,'','','Storming','7:10'),(4,'Trip Hazard','hazard4','2018-01-29',-27.475869,153.028253,'','','Snowing','7:15'),(5,'Trip Hazard','hazard5','2018-01-03',-27.477184,153.028599,'','','Clear','8:00'),(6,'Trip Hazard','hazard6','2018-09-08',-27.477267,153.028387,'','','Storming','9:35'),(7,'Slip Hazard','hazard7','2018-09-13',-27.476163,153.028737,'','','Raining','17:30'),(8,'Slip Hazard','hazard8','2018-09-18',-27.476448,153.028289,'','','Raining','8:55'),(9,'Trip Hazard','hazard9','2018-04-23',-27.475901,153.02843,'','','Overcast','9:55'),(10,'Slip Hazard','hazard10','2018-09-28',-27.476519,153.027414,'','','Raining','10:50'),(11,'Trip Hazard','hazard11','2018-05-03',-27.47616,153.027495,'','','Clear','12:30'),(12,'Slip Hazard','hazard12','2018-09-09',-27.476993,153.028347,'','','Storming','13:00'),(13,'Trip Hazard','hazard13','2018-09-10',-27.47705,153.02739,'','','Storming','13:10'),(14,'Trip Hazard','hazard14','2018-09-14',-27.476219,153.028756,'','','Snowing','17:05'),(15,'Slip Hazard','hazard15','2018-09-18',-27.476353,153.028664,'','','Clear','17:20'),(16,'Slip Hazard','hazard16','2018-09-22',-27.476788,153.028082,'','','Snowing','18:15'),(17,'Trip Hazard','hazard17','2018-09-25',-27.476977,153.027574,'','','Snowing','21:05'),(18,'Slip Hazard','hazard18','2018-02-28',-27.475871,153.027578,'','','Storming','7:55'),(19,'Slip Hazard','hazard19','2018-10-09',-27.476037,153.028707,'','','Overcast','5:55'),(20,'Trip Hazard','hazard20','2018-11-14',-27.476384,153.02769,'','','Snowing','7:20'),(21,'Trip Hazard','hazard21','2018-10-18',-27.476397,153.028523,'','','Overcast','9:10'),(22,'Trip Hazard','hazard22','2018-10-20',-27.477034,153.028716,'','','Raining','10:30'),(23,'Slip Hazard','hazard23','2018-10-26',-27.476431,153.028795,'','','Clear','10:40'),(24,'Trip Hazard','hazard24','2018-10-29',-27.476529,153.027598,'','','Overcast','12:15'),(25,'Slip Hazard','hazard25','2018-10-31',-27.476988,153.028689,'','','Raining','12:50'),(26,'Slip Hazard','hazard26','2018-10-14',-27.477368,153.027936,'','','Overcast','17:40'),(27,'Slip Hazard','hazard27','2018-10-15',-27.476549,153.027952,'','','Overcast','18:15'),(28,'Trip Hazard','hazard28','2018-10-17',-27.476906,153.028739,'','','Snowing','18:40'),(29,'Slip Hazard','hazard29','2018-10-27',-27.475956,153.028257,'','','Snowing','5:05'),(30,'Trip Hazard','hazard30','2018-11-02',-27.476975,153.028553,'','','Overcast','7:50'),(31,'Trip Hazard','hazard31','2018-11-03',-27.477138,153.027373,'','','Clear','10:00'),(32,'Trip Hazard','hazard32','2018-11-04',-27.476941,153.027635,'','','Storming','10:50'),(33,'Trip Hazard','hazard33','2018-11-06',-27.476322,153.028466,'','','Storming','12:00'),(34,'Slip Hazard','hazard34','2018-11-08',-27.477244,153.028799,'','','Snowing','12:10'),(35,'Slip Hazard','hazard35','2018-11-12',-27.475912,153.028141,'','','Raining','14:55'),(36,'Trip Hazard','hazard36','2018-11-15',-27.477387,153.028784,'','','Overcast','16:30'),(37,'Slip Hazard','hazard37','2018-11-17',-27.477242,153.027891,'','','Snowing','19:40'),(38,'Trip Hazard','hazard38','2018-11-18',-27.476969,153.028886,'','','Raining','19:55'),(39,'Slip Hazard','hazard39','2018-11-27',-27.475905,153.027935,'','','Raining','11:50'),(40,'Trip Hazard','hazard40','2018-11-30',-27.476363,153.028396,'','','Raining','12:30'),(41,'Slip Hazard','hazard41','2018-12-02',-27.476906,153.028046,'','','Raining','12:40'),(42,'Slip Hazard','hazard42','2018-12-06',-27.476362,153.028396,'','','Storming','13:05'),(43,'Slip Hazard','hazard43','2018-12-10',-27.476932,153.027649,'','','Snowing','8:30'),(44,'Slip Hazard','hazard44','2018-12-15',-27.476481,153.028871,'','','Raining','9:15'),(45,'Slip Hazard','hazard45','2018-12-16',-27.476519,153.028278,'','','Raining','10:25'),(46,'Slip Hazard','hazard46','2018-12-19',-27.477343,153.027705,'','','Clear','11:05'),(47,'Trip Hazard','hazard47','2018-12-20',-27.477413,153.028107,'','','Storming','13:05'),(48,'Trip Hazard','hazard48','2018-12-21',-27.477262,153.027405,'','','Raining','16:15'),(49,'Slip Hazard','hazard49','2018-12-28',-27.476574,153.028049,'','','Clear','17:30'),(50,'Trip Hazard','hazard50','2018-12-31',-27.475932,153.027848,'','','Clear','19:00'),(51,'Trip Hazard','hazard51','2018-10-18',-27.476062,153.028593,'','','Snowing','20:10'),(52,'Trip Hazard','hazard52','2018-10-20',-27.476479,153.02746,'','','Storming','20:30'),(53,'Slip Hazard','hazard53','2018-10-24',-27.476533,153.028863,'','','Storming','19:55'),(54,'Trip Hazard','hazard54','2018-10-25',-27.476434,153.028577,'','','Storming','15:00'),(55,'Trip Hazard','hazard55','2018-10-26',-27.476522,153.027893,'','','Snowing','15:10'),(56,'Trip Hazard','hazard56','2018-10-27',-27.477245,153.028786,'','','Clear','15:30'),(57,'Trip Hazard','hazard57','2018-10-29',-27.476147,153.028536,'','','Snowing','15:35'),(58,'Trip Hazard','hazard58','2018-10-30',-27.477252,153.028061,'','','Snowing','15:45'),(59,'Slip Hazard','hazard59','2018-11-01',-27.475869,153.027282,'','','Storming','15:50'),(60,'Slip Hazard','hazard60','2018-11-05',-27.476924,153.027507,'','','Overcast','16:00'),(61,'Slip Hazard','hazard61','2018-11-10',-27.47627,153.02876,'','','Overcast','5:05'),(62,'Slip Hazard','hazard62','2018-11-18',-27.475954,153.028061,'','','Overcast','7:50'),(63,'Trip Hazard','hazard63','2018-11-21',-27.476711,153.028736,'','','Overcast','10:00'),(64,'Trip Hazard','hazard64','2018-11-23',-27.476,153.028687,'','','Snowing','10:50'),(65,'Trip Hazard','hazard65','2018-12-03',-27.47662,153.028145,'','','Raining','12:00'),(66,'Slip Hazard','hazard66','2018-12-04',-27.476923,153.027543,'','','Overcast','12:10'),(67,'Slip Hazard','hazard67','2018-12-07',-27.476509,153.028662,'','','Snowing','14:55'),(68,'Trip Hazard','hazard68','2018-12-08',-27.475973,153.028295,'','','Clear','16:30'),(69,'Trip Hazard','hazard69','2018-12-09',-27.476929,153.027444,'','','Snowing','19:40'),(70,'Slip Hazard','hazard70','2018-12-12',-27.476762,153.027415,'','','Raining','19:55'),(71,'Slip Hazard','hazard71','2018-12-13',-27.476208,153.027462,'','','Clear','18:15'),(72,'Trip Hazard','hazard72','2018-12-14',-27.477118,153.028468,'','','Raining','18:20'),(73,'Trip Hazard','hazard73','2018-12-15',-27.47658,153.028165,'','','Overcast','18:30'),(74,'Slip Hazard','hazard74','2018-12-18',-27.477006,153.028529,'','','Snowing','18:35'),(75,'Slip Hazard','hazard75','2018-12-31',-27.47659,153.028169,'','','Overcast','18:40'),(76,'Trip Hazard','hazard76','2018-10-18',-27.477158,153.027405,'','','Overcast','18:45'),(77,'Trip Hazard','hazard77','2018-10-20',-27.476838,153.027487,'','','Storming','18:50'),(78,'Slip Hazard','hazard78','2018-11-05',-27.47587,153.028076,'A201','2','Overcast','14:55'),(79,'Trip Hazard','hazard79','2018-12-13',-27.47587,153.028076,'A202','2','Overcast','11:05'),(80,'Trip Hazard','hazard80','2018-02-13',-27.47587,153.028076,'A103','1','Snowing','18:30'),(81,'Slip Hazard','hazard81','2018-09-18',-27.47646,153.027145,'J101','1','Overcast','13:05'),(82,'Slip Hazard','hazard82','2018-10-20',-27.47646,153.027145,'J202','2','Snowing','15:35'),(83,'Slip Hazard','hazard83','2018-10-15',-27.476555,153.027695,'G101','1','Clear','12:30'),(84,'Slip Hazard','hazard84','2018-12-31',-27.476555,153.027695,'G200','2','Snowing','18:45'),(85,'Trip Hazard','hazard85','2018-11-10',-27.476295,153.02742,'F201','2','Clear','9:35'),(86,'Trip Hazard','hazard86','2018-10-30',-27.47739,153.029984,'X201','2','Storming','11:50'),(87,'Trip Hazard','hazard87','2018-11-03',-27.476295,153.02742,'F202','2','Clear','10:50'),(88,'Slip Hazard','hazard88','2018-10-14',-27.477245,153.029037,'N201','2','Storming','15:10'),(89,'Slip Hazard','hazard89','2018-10-31',-27.477245,153.029037,'N202','2','Storming','17:30'),(90,'Trip Hazard','hazard90','2018-08-19',-27.477245,153.029037,'N103','1','Raining','15:50'),(91,'Slip Hazard','hazard91','2018-08-29',-27.476295,153.02742,'F103','1','Clear','12:50'),(92,'Trip Hazard','hazard92','2018-12-06',-27.47739,153.029984,'X102','1','Raining','7:50'),(93,'Trip Hazard','hazard93','2018-12-09',-27.476555,153.027695,'G100','1','Clear','10:30'),(94,'Trip Hazard','hazard94','2018-11-06',-27.476555,153.027695,'G200','2','Storming','12:10'),(95,'Trip Hazard','hazard95','2018-02-15',-27.476555,153.027695,'G300','3','Overcast','13:00'),(96,'Trip Hazard','hazard96','2018-12-07',-27.476555,153.027695,'G400','4','Storming','18:15'),(97,'Trip Hazard','hazard97','2018-12-28',-27.476555,153.027695,'G100','1','Overcast','18:20'),(98,'Slip Hazard','hazard98','2018-10-14',-27.476728,153.0271,'R202','2','Raining','14:55'),(99,'Trip Hazard','hazard99','2018-11-27',-27.476728,153.0271,'R203','2','Clear','5:45'),(100,'Slip Hazard','hazard100','2018-10-29',-27.476728,153.0271,'R204','2','Raining','5:55'),(101,'Trip Hazard','hazard101','2018-08-14',-27.477739,153.027634,'Z403','4','Raining','17:30'),(102,'Trip Hazard','hazard102','2018-10-26',-27.477739,153.027634,'Z401','4','Storming','5:05'),(103,'Trip Hazard','hazard103','2018-12-18',-27.47607,153.027725,'D202','2','Raining','16:15'),(104,'Slip Hazard','hazard104','2018-09-28',-27.47607,153.027725,'D203','2','Clear','7:50'),(105,'Trip Hazard','hazard105','2018-09-03',-27.47607,153.027725,'D204','2','Clear','5:05'),(106,'Slip Hazard','hazard106','2018-04-10',-27.47607,153.027725,'D205','2','Overcast','20:10'),(107,'Trip Hazard','hazard107','2018-09-09',-27.47607,153.027725,'D206','2','Raining','12:40'),(108,'Trip Hazard','hazard108','2018-09-14',-27.477308,153.027176,'S101','1','Raining','7:15'),(109,'Slip Hazard','hazard109','2018-12-02',-27.477308,153.027176,'S201','2','Storming','9:15'),(110,'Slip Hazard','hazard110','2018-04-24',-27.477308,153.027176,'S301','3','Clear','18:40'),(111,'Slip Hazard','hazard111','2018-11-23',-27.477308,153.027176,'S401','4','Overcast','20:30'),(112,'Trip Hazard','hazard112','2018-04-25',-27.477308,153.027176,'S501','5','Raining','19:55'),(113,'Trip Hazard','hazard113','2018-11-01',-27.477308,153.027176,'S601','6','Storming','19:00'),(114,'Slip Hazard','hazard114','2018-12-20',-27.477308,153.027176,'S701','7','Raining','19:55'),(115,'Slip Hazard','hazard115','2018-10-03',-27.477308,153.027176,'S801','8','Overcast','19:40'),(116,'Trip Hazard','hazard116','2016-10-26',-27.477308,153.027176,'S901','9','Snowing','9:10'),(117,'Trip Hazard','hazard117','2018-12-04',-27.477875,153.028122,'OP402','4','Clear','18:15'),(118,'Slip Hazard','hazard118','2018-12-03',-27.477875,153.028122,'OP303','3','Raining','10:00'),(119,'Slip Hazard','hazard119','2018-10-20',-27.477749,153.028183,'O101','1','Clear','12:00'),(120,'Slip Hazard','hazard120','2018-12-19',-27.477749,153.028183,'O202','2','Clear','13:05'),(121,'Trip Hazard','hazard121','2018-09-08',-27.477749,153.028183,'O303','3','Storming','18:35'),(122,'Trip Hazard','hazard122','2018-05-18',-27.477749,153.028183,'O404','4','Storming','8:30'),(123,'Slip Hazard','hazard123','2018-11-08',-27.477154,153.027542,'Q101','1','Storming','16:00'),(124,'Trip Hazard','hazard124','2016-12-08',-27.477154,153.027542,'Q202','2','Storming','10:25'),(125,'Slip Hazard','hazard125','2018-11-12',-27.477154,153.027542,'Q303','3','Snowing','10:50'),(126,'Slip Hazard','hazard126','2018-10-27',-27.476812,153.027893,'H101','1','Clear','12:15'),(127,'Trip Hazard','hazard127','2018-11-02',-27.476812,153.027893,'H202','2','Snowing','17:20'),(128,'Slip Hazard','hazard128','2017-11-18',-27.476582,153.026901,'W101','1','Snowing','19:40'),(129,'Slip Hazard','hazard129','2017-12-16',-27.476582,153.026901,'W202','2','Raining','15:00'),(130,'Trip Hazard','hazard130','2018-10-27',-27.476582,153.026901,'W303','3','Raining','9:55'),(131,'Trip Hazard','hazard131','2018-12-31',-27.476812,153.027893,'H202','2','Snowing','10:40'),(132,'Trip Hazard','hazard132','2017-11-04',-27.476812,153.027893,'H222','2','Snowing','13:10'),(133,'Trip Hazard','hazard133','2018-09-23',-27.476812,153.027893,'H310','3','Overcast','10:50'),(134,'Slip Hazard','hazard134','2018-10-24',-27.476812,153.027893,'H401','4','Overcast','15:45'),(135,'Trip Hazard','hazard135','2018-10-20',-27.477154,153.027542,'Q111','1','Clear','17:40'),(136,'Slip Hazard','hazard136','2017-11-30',-27.476812,153.027893,'H523','5','Storming','16:30'),(137,'Trip Hazard','hazard137','2018-10-17',-27.476812,153.027893,'H312','3','Snowing','5:05'),(138,'Trip Hazard','hazard138','2018-09-22',-27.477154,153.027542,'Q123','1','Overcast','8:00'),(139,'Slip Hazard','hazard139','2017-12-10',-27.476582,153.026901,'W123','1','Overcast','21:05'),(140,'Slip Hazard','hazard140','2018-10-18',-27.476582,153.026901,'W143','1','Clear','17:05'),(141,'Slip Hazard','hazard141','2018-10-25',-27.476582,153.026901,'W154','1','Clear','8:55'),(142,'Slip Hazard','hazard142','2018-09-18',-27.476582,153.026901,'W123','1','Overcast','15:30'),(143,'Trip Hazard','hazard143','2018-12-12',-27.476582,153.026901,'W202','2','Storming','12:00'),(144,'Trip Hazard','hazard144','2018-10-18',-27.476582,153.026901,'W111','1','Raining','12:10'),(145,'Trip Hazard','hazard145','2018-12-15',-27.477245,153.029037,'N303','3','Snowing','16:30'),(146,'Slip Hazard','hazard146','2018-12-16',-27.477245,153.029037,'N302','3','Snowing','12:30'),(147,'Trip Hazard','hazard147','2017-09-28',-27.477245,153.029037,'N301','3','Snowing','18:50'),(148,'Slip Hazard','hazard148','2018-11-17',-27.477154,153.027542,'Q301','3','Snowing','7:20'),(149,'Slip Hazard','hazard149','2018-11-21',-27.477245,153.029037,'N202','2','Raining','10:00'),(150,'Trip Hazard','hazard150','2018-10-29',-27.47646,153.027145,'J102','1','Clear','19:55'),(151,'Slip Hazard','hazard151','2018-11-15',-27.477245,153.029037,'N303','3','Clear','18:40'),(152,'Trip Hazard','hazard152','2018-11-18',-27.477245,153.029037,'N401','4','Storming','7:10'),(153,'Trip Hazard','hazard153','2017-12-14',-27.47646,153.027145,'J301','3','Overcast','18:15'),(154,'Slip Hazard','hazard154','2018-10-09',-27.477154,153.027542,'Q301','3','Snowing','7:55');
/*!40000 ALTER TABLE `qutheatmap_hazardtable` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `qutheatmap_imgtable`
--

DROP TABLE IF EXISTS `qutheatmap_imgtable`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `qutheatmap_imgtable` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `block` varchar(50) NOT NULL,
  `block_level` varchar(50) NOT NULL,
  `img_url` varchar(500) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qutheatmap_imgtable`
--

LOCK TABLES `qutheatmap_imgtable` WRITE;
/*!40000 ALTER TABLE `qutheatmap_imgtable` DISABLE KEYS */;
INSERT INTO `qutheatmap_imgtable` VALUES (1,'A','1','../static/images/GPfloorplans/ABlock/level1.png'),(2,'A','2','../static/images/GPfloorplans/ABlock/level2.png'),(3,'A','3','../static/images/GPfloorplans/ABlock/level3.png'),(4,'W','1','../static/images/GPfloorplans/WBlock/level1.png'),(5,'W','2','../static/images/GPfloorplans/WBlock/level2.png'),(6,'W','3','../static/images/GPfloorplans/WBlock/level3.png'),(7,'W','4','../static/images/GPfloorplans/WBlock/level4.png'),(8,'N','0','../static/images/GPfloorplans/NBlock/level0.png'),(9,'N','1','../static/images/GPfloorplans/NBlock/level1.png'),(10,'N','2','../static/images/GPfloorplans/NBlock/level2.png'),(11,'N','3','../static/images/GPfloorplans/NBlock/level3.png'),(12,'S','1','../static/images/GPfloorplans/SBlock/level1.png'),(13,'S','2','../static/images/GPfloorplans/SBlock/level2.png'),(14,'S','3','../static/images/GPfloorplans/SBlock/level3.png'),(15,'S','4','../static/images/GPfloorplans/SBlock/level4.png'),(16,'S','5','../static/images/GPfloorplans/SBlock/level5.png'),(17,'S','6','../static/images/GPfloorplans/SBlock/level6.png'),(18,'S','7','../static/images/GPfloorplans/SBlock/level7.png'),(19,'S','8','../static/images/GPfloorplans/SBlock/level8.png'),(20,'S','9','../static/images/GPfloorplans/SBlock/level9.png'),(21,'S','10','../static/images/GPfloorplans/SBlock/level10.png'),(22,'S','11','../static/images/GPfloorplans/SBlock/level11.png'),(23,'S','12','../static/images/GPfloorplans/SBlock/level12.png'),(24,'S','13','../static/images/GPfloorplans/SBlock/level13.png'),(25,'S','14','../static/images/GPfloorplans/SBlock/level14.png'),(26,'S','15','../static/images/GPfloorplans/SBlock/level15.png'),(27,'R','1','../static/images/GPfloorplans/RBlock/level1.png'),(28,'R','2','../static/images/GPfloorplans/RBlock/level2.png'),(29,'R','3','../static/images/GPfloorplans/RBlock/level3.png'),(30,'R','4','../static/images/GPfloorplans/RBlock/level4.png'),(31,'G','1','../static/images/GPfloorplans/GBlock/level1.png'),(32,'G','2','../static/images/GPfloorplans/GBlock/level2.png'),(33,'G','3','../static/images/GPfloorplans/GBlock/level3.png'),(34,'G','4','../static/images/GPfloorplans/GBlock/level4.png'),(35,'B','1','../static/images/GPfloorplans/BBlock/level1.png'),(36,'B','2','../static/images/GPfloorplans/BBlock/level2.png'),(37,'B','3','../static/images/GPfloorplans/BBlock/level3.png'),(38,'B','4','../static/images/GPfloorplans/BBlock/level4.png'),(39,'B','5','../static/images/GPfloorplans/BBlock/level5.png'),(40,'B','6','../static/images/GPfloorplans/BBlock/level6.png'),(41,'B','7','../static/images/GPfloorplans/BBlock/level7.png'),(42,'C','1','../static/images/GPfloorplans/CBlock/level1.png'),(43,'C','2','../static/images/GPfloorplans/CBlock/level2.png'),(44,'C','3','../static/images/GPfloorplans/CBlock/level3.png'),(45,'C','4','../static/images/GPfloorplans/CBlock/level4.png'),(46,'C','5','../static/images/GPfloorplans/CBlock/level5.png'),(47,'C','6','../static/images/GPfloorplans/CBlock/level6.png'),(48,'C','7','../static/images/GPfloorplans/CBlock/level7.png'),(49,'C','8','../static/images/GPfloorplans/CBlock/level8.png'),(50,'J','1','../static/images/GPfloorplans/JBlock/level1.png'),(51,'J','2','../static/images/GPfloorplans/JBlock/level2.png'),(52,'H','1','../static/images/GPfloorplans/HBlock/level1.png'),(53,'H','2','../static/images/GPfloorplans/HBlock/level2.png'),(54,'H','3','../static/images/GPfloorplans/HBlock/level3.png'),(55,'H','4','../static/images/GPfloorplans/HBlock/level4.png'),(56,'Z','1','../static/images/GPfloorplans/ZBlock/level1.png'),(57,'Z','2','../static/images/GPfloorplans/ZBlock/level2.png'),(58,'Z','3','../static/images/GPfloorplans/ZBlock/level3.png'),(59,'Z','4','../static/images/GPfloorplans/ZBlock/level4.png'),(60,'Z','5','../static/images/GPfloorplans/ZBlock/level5.png'),(61,'Z','6','../static/images/GPfloorplans/ZBlock/level6.png'),(62,'Z','7','../static/images/GPfloorplans/ZBlock/level7.png'),(63,'Z','8','../static/images/GPfloorplans/ZBlock/level8.png'),(64,'Z','9','../static/images/GPfloorplans/ZBlock/level9.png'),(65,'Z','10','../static/images/GPfloorplans/ZBlock/level10.png'),(66,'Z','11','../static/images/GPfloorplans/ZBlock/level11.png'),(67,'Z','12','../static/images/GPfloorplans/ZBlock/level12.png'),(68,'Z','13','../static/images/GPfloorplans/ZBlock/level13.png');
/*!40000 ALTER TABLE `qutheatmap_imgtable` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `qutheatmap_injurytable`
--

DROP TABLE IF EXISTS `qutheatmap_injurytable`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `qutheatmap_injurytable` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `description` varchar(250) NOT NULL,
  `incident_date` varchar(50) NOT NULL,
  `lat` double NOT NULL,
  `lng` double NOT NULL,
  `room_number` varchar(25) NOT NULL,
  `floor` varchar(10) NOT NULL,
  `weather` varchar(50) NOT NULL,
  `time` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=155 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qutheatmap_injurytable`
--

LOCK TABLES `qutheatmap_injurytable` WRITE;
/*!40000 ALTER TABLE `qutheatmap_injurytable` DISABLE KEYS */;
INSERT INTO `qutheatmap_injurytable` VALUES (1,'Trip Injury','testing1','2014-03-20',-27.476075,153.028013,'','','Raining','5:05'),(2,'Fall Injury','testing2','2016-02-11',-27.477103,153.028147,'','','Storming','5:45'),(3,'Trip Injury','testing3','2015-08-31',-27.476427,153.028305,'','','Overcast','7:10'),(4,'Fall Injury','testing4','2015-12-10',-27.476968,153.028462,'','','Clear','7:15'),(5,'Slip Injury','testing5','2016-07-15',-27.476386,153.027678,'','','Overcast','8:00'),(6,'Trip Injury','testing6','2015-11-24',-27.477045,153.028709,'','','Snowing','9:35'),(7,'Trip Injury','testing7','2015-05-20',-27.476345,153.027947,'','','Overcast','17:30'),(8,'Slip Injury','testing8','2014-04-24',-27.476692,153.028271,'','','Snowing','8:55'),(9,'Fall Injury','testing9','2014-11-07',-27.47701,153.028352,'','','Snowing','9:55'),(10,'Trip Injury','testing10','2014-11-26',-27.47691,153.027333,'','','Overcast','10:50'),(11,'Fall Injury','testing11','2015-01-23',-27.476779,153.028582,'','','Storming','12:30'),(12,'Trip Injury','testing12','2015-03-27',-27.476275,153.028832,'','','Storming','13:00'),(13,'Slip Injury','testing13','2016-10-25',-27.477226,153.028092,'','','Raining','13:10'),(14,'Trip Injury','testing14','2016-03-17',-27.47706,153.028695,'','','Storming','17:05'),(15,'Slip Injury','testing15','2018-02-26',-27.476619,153.028739,'','','Overcast','17:20'),(16,'Slip Injury','testing16','2018-05-01',-27.476618,153.027811,'','','Overcast','18:15'),(17,'Slip Injury','testing17','2018-04-18',-27.476207,153.028491,'','','Snowing','21:05'),(18,'Fall Injury','testing18','2018-09-28',-27.476351,153.02748,'','','Storming','7:55'),(19,'Slip Injury','testing19','2018-06-08',-27.476871,153.02882,'','','Clear','5:55'),(20,'Trip Injury','testing20','2018-10-14',-27.476312,153.02786,'','','Storming','7:20'),(21,'Trip Injury','testing21','2018-12-27',-27.476061,153.027962,'','','Clear','9:10'),(22,'Slip Injury','testing22','2018-12-03',-27.476088,153.02804,'','','Storming','10:30'),(23,'Fall Injury','testing23','2018-01-08',-27.476124,153.02838,'','','Overcast','10:40'),(24,'Fall Injury','testing24','2018-01-19',-27.477271,153.028404,'','','Clear','12:15'),(25,'Slip Injury','testing25','2018-02-16',-27.47732,153.028155,'','','Raining','12:50'),(26,'Slip Injury','testing26','2018-02-22',-27.476228,153.028183,'','','Storming','17:40'),(27,'Trip Injury','testing27','2018-02-27',-27.476768,153.028755,'','','Raining','18:15'),(28,'Trip Injury','testing28','2018-03-06',-27.475983,153.027875,'','','Raining','18:40'),(29,'Trip Injury','testing29','2018-03-09',-27.476997,153.028073,'','','Clear','5:05'),(30,'Fall Injury','testing30','2018-04-12',-27.47708,153.028595,'','','Raining','7:50'),(31,'Trip Injury','testing31','2018-09-21',-27.476261,153.028725,'','','Raining','10:00'),(32,'Fall Injury','testing32','2018-08-13',-27.477026,153.027401,'','','Storming','10:50'),(33,'Trip Injury','testing33','2018-07-05',-27.47624,153.028044,'','','Raining','12:00'),(34,'Fall Injury','testing34','2018-06-06',-27.476372,153.027705,'','','Clear','12:10'),(35,'Slip Injury','testing35','2018-11-12',-27.476944,153.028786,'','','Raining','14:55'),(36,'Trip Injury','testing36','2018-11-15',-27.47661,153.028548,'','','Snowing','16:30'),(37,'Trip Injury','testing37','2018-11-17',-27.47653,153.02795,'','','Overcast','19:40'),(38,'Slip Injury','testing38','2018-11-18',-27.476497,153.027437,'','','Storming','19:55'),(39,'Fall Injury','testing39','2018-11-27',-27.476221,153.028471,'','','Overcast','11:50'),(40,'Trip Injury','testing40','2018-11-30',-27.476176,153.027587,'','','Snowing','12:30'),(41,'Fall Injury','testing41','2018-12-02',-27.476115,153.028748,'','','Storming','12:40'),(42,'Trip Injury','testing42','2018-12-06',-27.476202,153.027677,'','','Raining','13:05'),(43,'Slip Injury','testing43','2018-12-10',-27.476849,153.027354,'','','Snowing','8:30'),(44,'Trip Injury','testing44','2018-12-15',-27.47694,153.028421,'','','Overcast','9:15'),(45,'Slip Injury','testing45','2018-12-16',-27.476818,153.028256,'','','Overcast','10:25'),(46,'Slip Injury','testing46','2018-05-02',-27.477146,153.02834,'','','Clear','11:05'),(47,'Slip Injury','testing47','2018-12-20',-27.476031,153.0273,'','','Clear','13:05'),(48,'Fall Injury','testing48','2018-12-21',-27.476424,153.027433,'','','Raining','16:15'),(49,'Slip Injury','testing49','2018-12-28',-27.47705,153.028426,'','','Storming','17:30'),(50,'Trip Injury','testing50','2018-12-31',-27.476453,153.02837,'','','Storming','19:00'),(51,'Trip Injury','testing51','2018-08-08',-27.47619,153.028565,'','','Snowing','20:10'),(52,'Slip Injury','testing52','2018-11-02',-27.477116,153.02848,'','','Overcast','20:30'),(53,'Fall Injury','testing53','2018-10-24',-27.476747,153.028681,'','','Snowing','19:55'),(54,'Fall Injury','testing54','2018-10-25',-27.477245,153.027408,'','','Clear','15:00'),(55,'Slip Injury','testing55','2018-10-26',-27.476089,153.028782,'','','Storming','15:10'),(56,'Slip Injury','testing56','2018-10-27',-27.476857,153.028158,'','','Clear','15:30'),(57,'Trip Injury','testing57','2018-10-29',-27.476322,153.027463,'','','Storming','15:35'),(58,'Trip Injury','testing58','2018-08-08',-27.477393,153.027667,'','','Raining','15:45'),(59,'Trip Injury','testing59','2018-01-05',-27.477399,153.027267,'','','Storming','15:50'),(60,'Fall Injury','testing60','2018-11-05',-27.476019,153.027942,'','','Clear','16:00'),(61,'Trip Injury','testing61','2018-01-31',-27.476622,153.02788,'','','Overcast','5:05'),(62,'Fall Injury','testing62','2018-07-19',-27.476235,153.028649,'','','Snowing','7:50'),(63,'Trip Injury','testing63','2018-02-05',-27.476306,153.027301,'','','Overcast','10:00'),(64,'Fall Injury','testing64','2018-11-23',-27.477335,153.028077,'','','Storming','10:50'),(65,'Slip Injury','testing65','2018-12-03',-27.476007,153.028608,'','','Snowing','12:00'),(66,'Trip Injury','testing66','2018-08-28',-27.476659,153.028627,'','','Overcast','12:10'),(67,'Trip Injury','testing67','2018-04-13',-27.476783,153.027333,'','','Clear','14:55'),(68,'Slip Injury','testing68','2018-12-08',-27.476015,153.028654,'','','Raining','16:30'),(69,'Fall Injury','testing69','2018-06-25',-27.476247,153.028736,'','','Snowing','19:40'),(70,'Trip Injury','testing70','2018-07-26',-27.477101,153.028793,'','','Overcast','19:55'),(71,'Fall Injury','testing71','2018-10-15',-27.47643,153.028185,'','','Raining','18:15'),(72,'Trip Injury','testing72','2018-12-14',-27.476806,153.028847,'','','Storming','18:20'),(73,'Slip Injury','testing73','2018-09-06',-27.476176,153.027692,'','','Raining','18:30'),(74,'Trip Injury','testing74','2018-06-25',-27.476048,153.027794,'','','Overcast','18:35'),(75,'Slip Injury','testing75','2018-06-14',-27.477116,153.027557,'','','Snowing','18:40'),(76,'Slip Injury','testing76','2018-10-18',-27.476402,153.028415,'','','Clear','18:45'),(77,'Slip Injury','testing77','2018-10-20',-27.477284,153.027644,'','','Snowing','18:50'),(78,'Fall Injury','placeholder1','2018-08-14',-27.47587,153.028076,'A101','1','Snowing','5:05'),(79,'Slip Injury','placeholder2','2018-08-19',-27.47587,153.028076,'A102','1','Clear','5:45'),(80,'Trip Injury','placeholder3','2018-08-24',-27.47587,153.028076,'A103','1','Clear','7:10'),(81,'Trip Injury','placeholder4','2018-08-29',-27.47646,153.027145,'J201','2','Clear','7:15'),(82,'Slip Injury','placeholder5','2018-09-03',-27.47646,153.027145,'J202','2','Storming','8:00'),(83,'Fall Injury','placeholder6','2018-09-08',-27.476555,153.027695,'G101','1','Storming','9:35'),(84,'Fall Injury','placeholder7','2018-09-13',-27.476555,153.027695,'G102','1','Storming','17:30'),(85,'Slip Injury','placeholder8','2018-09-18',-27.476295,153.02742,'F101','1','Overcast','8:55'),(86,'Slip Injury','placeholder9','2018-09-23',-27.47739,153.029984,'X101','1','Clear','9:55'),(87,'Trip Injury','placeholder10','2018-09-28',-27.476295,153.02742,'F102','1','Snowing','10:50'),(88,'Trip Injury','placeholder11','2018-10-03',-27.477245,153.029037,'N101','1','Clear','12:30'),(89,'Trip Injury','placeholder12','2018-09-09',-27.477245,153.029037,'N102','1','Storming','13:00'),(90,'Fall Injury','placeholder13','2018-09-10',-27.477245,153.029037,'N103','1','Snowing','13:10'),(91,'Trip Injury','placeholder14','2018-09-14',-27.476295,153.02742,'F103','1','Storming','17:05'),(92,'Fall Injury','placeholder15','2018-09-18',-27.47739,153.029984,'X102','1','Storming','17:20'),(93,'Trip Injury','placeholder16','2018-09-22',-27.476555,153.027695,'G102','1','Raining','18:15'),(94,'Fall Injury','placeholder17','2018-09-25',-27.476555,153.027695,'G103','1','Raining','21:05'),(95,'Slip Injury','placeholder18','2018-09-28',-27.476555,153.027695,'G104','1','Raining','7:55'),(96,'Trip Injury','placeholder19','2018-10-09',-27.476555,153.027695,'G105','1','Storming','5:55'),(97,'Trip Injury','placeholder20','2018-10-14',-27.476555,153.027695,'G106','1','Raining','7:20'),(98,'Slip Injury','placeholder21','2018-10-18',-27.476728,153.0271,'R202','2','Overcast','9:10'),(99,'Fall Injury','placeholder22','2018-10-20',-27.476728,153.0271,'R203','2','Raining','10:30'),(100,'Trip Injury','placeholder23','2018-10-26',-27.476728,153.0271,'R204','2','Raining','10:40'),(101,'Fall Injury','placeholder24','2018-10-29',-27.477739,153.027634,'Z403','4','Raining','12:15'),(102,'Trip Injury','placeholder25','2018-10-31',-27.477739,153.027634,'Z403','4','Raining','12:50'),(103,'Slip Injury','placeholder26','2018-10-14',-27.47607,153.027725,'D202','2','Clear','17:40'),(104,'Trip Injury','placeholder27','2018-10-15',-27.47607,153.027725,'D203','2','Clear','18:15'),(105,'Slip Injury','placeholder28','2018-10-17',-27.47607,153.027725,'D204','2','Overcast','18:40'),(106,'Slip Injury','placeholder29','2018-10-27',-27.47607,153.027725,'D205','2','Snowing','5:05'),(107,'Slip Injury','placeholder30','2018-11-02',-27.47607,153.027725,'D206','2','Overcast','7:50'),(108,'Fall Injury','placeholder31','2018-11-03',-27.47801,153.028931,'P401','4','Storming','10:00'),(109,'Slip Injury','placeholder32','2018-11-04',-27.47801,153.028931,'P402','4','Overcast','10:50'),(110,'Trip Injury','placeholder33','2018-11-06',-27.47801,153.028931,'P403','4','Clear','12:00'),(111,'Trip Injury','placeholder34','2018-11-08',-27.47801,153.028931,'P404','4','Overcast','12:10'),(112,'Slip Injury','placeholder35','2018-11-12',-27.47801,153.028931,'P405','4','Clear','14:55'),(113,'Fall Injury','placeholder36','2018-11-15',-27.47801,153.028931,'P406','4','Raining','16:30'),(114,'Fall Injury','placeholder37','2018-11-17',-27.47801,153.028931,'P407','4','Raining','19:40'),(115,'Slip Injury','placeholder38','2018-11-18',-27.47801,153.028931,'P408','4','Snowing','19:55'),(116,'Slip Injury','placeholder39','2018-11-27',-27.47801,153.028931,'P409','4','Clear','11:50'),(117,'Trip Injury','placeholder40','2018-11-30',-27.477875,153.028122,'OP202','2','Storming','12:30'),(118,'Trip Injury','placeholder41','2018-12-02',-27.477875,153.028122,'OP203','2','Snowing','12:40'),(119,'Trip Injury','placeholder42','2018-12-06',-27.477749,153.028183,'O101','1','Storming','13:05'),(120,'Fall Injury','placeholder43','2018-12-10',-27.477749,153.028183,'O202','2','Overcast','8:30'),(121,'Trip Injury','placeholder44','2018-12-15',-27.477749,153.028183,'O303','3','Raining','9:15'),(122,'Fall Injury','placeholder45','2018-12-16',-27.477749,153.028183,'O404','4','Snowing','10:25'),(123,'Trip Injury','placeholder46','2018-12-19',-27.477154,153.027542,'Q101','1','Snowing','11:05'),(124,'Fall Injury','placeholder47','2018-12-20',-27.477154,153.027542,'Q202','2','Clear','13:05'),(125,'Slip Injury','placeholder48','2018-12-21',-27.477154,153.027542,'Q303','3','Overcast','16:15'),(126,'Trip Injury','placeholder49','2018-12-28',-27.476812,153.027893,'H101','1','Raining','17:30'),(127,'Trip Injury','placeholder50','2018-01-02',-27.476812,153.027893,'H202','2','Storming','19:00'),(128,'Slip Injury','placeholder51','2018-01-08',-27.476582,153.026901,'W101','1','Snowing','20:10'),(129,'Fall Injury','placeholder52','2018-01-26',-27.476582,153.026901,'W202','2','Clear','20:30'),(130,'Trip Injury','placeholder53','2018-02-26',-27.476582,153.026901,'W303','3','Snowing','19:55'),(131,'Fall Injury','placeholder54','2018-03-26',-27.476812,153.027893,'H202','2','Clear','15:00'),(132,'Trip Injury','placeholder55','2018-04-05',-27.476812,153.027893,'H222','2','Storming','15:10'),(133,'Slip Injury','placeholder56','2018-04-19',-27.476812,153.027893,'H310','3','Storming','15:30'),(134,'Trip Injury','placeholder57','2018-05-02',-27.476812,153.027893,'H401','4','Snowing','15:35'),(135,'Slip Injury','placeholder58','2018-04-24',-27.477154,153.027542,'Q111','1','Snowing','15:45'),(136,'Slip Injury','placeholder59','2018-05-07',-27.476812,153.027893,'H523','5','Storming','15:50'),(137,'Slip Injury','placeholder60','2018-07-20',-27.476812,153.027893,'H312','3','Storming','16:00'),(138,'Fall Injury','placeholder61','2018-07-30',-27.477154,153.027542,'Q123','1','Raining','5:05'),(139,'Slip Injury','placeholder62','2018-08-09',-27.476582,153.026901,'W123','1','Clear','7:50'),(140,'Trip Injury','placeholder63','2018-08-16',-27.476582,153.026901,'W143','1','Snowing','10:00'),(141,'Trip Injury','placeholder64','2018-08-29',-27.476582,153.026901,'W154','1','Overcast','10:50'),(142,'Slip Injury','placeholder65','2018-09-10',-27.476582,153.026901,'W123','1','Overcast','12:00'),(143,'Fall Injury','placeholder66','2018-09-24',-27.476582,153.026901,'W202','2','Overcast','12:10'),(144,'Fall Injury','placeholder67','2018-10-01',-27.476582,153.026901,'W111','1','Storming','14:55'),(145,'Slip Injury','placeholder68','2018-10-11',-27.477245,153.029037,'N303','3','Snowing','16:30'),(146,'Slip Injury','placeholder69','2018-10-15',-27.477245,153.029037,'N302','3','Clear','19:40'),(147,'Trip Injury','placeholder70','2018-01-04',-27.477245,153.029037,'N301','3','Raining','19:55'),(148,'Trip Injury','placeholder71','2018-03-19',-27.477154,153.027542,'Q301','3','Raining','18:15'),(149,'Trip Injury','placeholder72','2018-08-31',-27.477245,153.029037,'N202','2','Storming','18:20'),(150,'Fall Injury','placeholder73','2018-07-02',-27.47646,153.027145,'J502','5','Snowing','18:30'),(151,'Trip Injury','placeholder74','2018-12-18',-27.477245,153.029037,'N303','3','Overcast','18:35'),(152,'Fall Injury','placeholder75','2018-04-26',-27.477245,153.029037,'N401','4','Clear','18:40'),(153,'Trip Injury','placeholder76','2018-03-26',-27.47646,153.027145,'J301','3','Clear','18:45'),(154,'Fall Injury','placeholder77','2018-10-20',-27.477154,153.027542,'Q301','3','Storming','18:50');
/*!40000 ALTER TABLE `qutheatmap_injurytable` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `qutheatmap_markers`
--

DROP TABLE IF EXISTS `qutheatmap_markers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `qutheatmap_markers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `building_code` varchar(10) NOT NULL,
  `block` varchar(10) NOT NULL,
  `lat` float(10,6) NOT NULL,
  `lng` float(10,6) NOT NULL,
  `description` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qutheatmap_markers`
--

LOCK TABLES `qutheatmap_markers` WRITE;
/*!40000 ALTER TABLE `qutheatmap_markers` DISABLE KEYS */;
INSERT INTO `qutheatmap_markers` VALUES (1,'GP101','A',-27.475870,153.028076,'A Block'),(2,'GP102','B',-27.476185,153.028442,'B Block'),(3,'GP128','C',-27.478271,153.027893,'C Block'),(4,'GP112','D',-27.476070,153.027725,'D Block'),(5,'GP111','E',-27.476576,153.028152,'E Block'),(6,'GP113','F',-27.476295,153.027420,'F Block'),(7,'GP114','G',-27.476555,153.027695,'G Block'),(8,'GP115','H',-27.476812,153.027893,'H Block'),(9,'GP117','J',-27.476460,153.027145,'J Block'),(10,'GP119','M',-27.477392,153.027924,'M Block'),(11,'GP105','N',-27.477245,153.029037,'N Block'),(12,'GP120','O',-27.477749,153.028183,'O Block'),(13,'GP220','OP',-27.477875,153.028122,'O Podium'),(14,'GP140','P',-27.478010,153.028931,'P Block'),(15,'GP118','Q',-27.477154,153.027542,'Q Block'),(16,'GP126','R',-27.476728,153.027100,'R Block'),(17,'GP129','S',-27.477308,153.027176,'S Block'),(18,'GP103','U',-27.476593,153.028824,'U Block'),(19,'GP110','V',-27.477022,153.028366,'V Block'),(20,'GP124','W',-27.476582,153.026901,'W Block'),(21,'GP106','X',-27.477390,153.029984,'X Block'),(22,'GP141','Y',-27.477612,153.029724,'Y Block'),(23,'GP130','Z',-27.477739,153.027634,'Z Block');
/*!40000 ALTER TABLE `qutheatmap_markers` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-10-29 14:17:57
