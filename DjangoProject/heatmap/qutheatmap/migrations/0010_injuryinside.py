# Generated by Django 2.0.4 on 2018-05-10 01:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('qutheatmap', '0009_delete_hazardoutside'),
    ]

    operations = [
        migrations.CreateModel(
            name='InjuryInside',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=50)),
                ('description', models.CharField(max_length=250)),
                ('incident_date', models.DateField()),
                ('lat', models.FloatField(default=0.0, max_length=25)),
                ('lng', models.FloatField(default=0.0, max_length=25)),
                ('room_number', models.CharField(max_length=25)),
                ('floor', models.CharField(max_length=10)),
            ],
        ),
    ]
