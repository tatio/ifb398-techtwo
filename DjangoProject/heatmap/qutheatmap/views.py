from django.shortcuts import render, HttpResponse
from qutheatmap.models import Markers, HazardTable, InjuryTable, ImgTable, Aeds, HandicapLocation

# Create your views here.


def home(request):
    marker = Markers.objects.all()
    injurytables = InjuryTable.objects.all()
    hazardtables = HazardTable.objects.all()
    imgtables = ImgTable.objects.all()
    aeds = Aeds.objects.all()
    handicap = HandicapLocation.objects.all()

    mapdata = {
        'markers': marker,
        'injurytables': injurytables,
        'hazardtables': hazardtables,
        'imgtables': imgtables,
        'aeds': aeds,
        'handicap': handicap
    }

    return render(request, 'heatmap/map.html', mapdata)

def printpage(request):
    marker = Markers.objects.all()
    injurytables = InjuryTable.objects.all()
    hazardtables = HazardTable.objects.all()
    imgtables = ImgTable.objects.all()

    mapdata = {
        'markers': marker,
        'injurytables': injurytables,
        'hazardtables': hazardtables,
        'imgtables': imgtables
    }

    return render(request, 'heatmap/print-overview.html', mapdata)
