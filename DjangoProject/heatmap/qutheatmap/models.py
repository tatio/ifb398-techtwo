from django.db import models

# Create your models here.


class Markers(models.Model):

    # building_id, building_code, block, lat, lng
    building_code = models.CharField(max_length=10)
    block = models.CharField(max_length=10)
    lat = models.FloatField(max_length=25, default=0.00000)
    lng = models.FloatField(max_length=25, default=0.00000)
    description = models.CharField(max_length=250)

    def __unicode__(self):
        return self.building_code

class Aeds(models.Model):

    description = models.CharField(max_length=250)
    lat = models.FloatField(max_length=25, default=0.00000)
    lng = models.FloatField(max_length=25, default=0.00000)

    def __unicode__(self):
        return self.description

class HandicapLocation(models.Model):

    description = models.CharField(max_length=250)
    lat = models.FloatField(max_length=25, default=0.00000)
    lng = models.FloatField(max_length=25, default=0.00000)

    def __unicode__(self):
        return self.description

class InjuryTable(models.Model):

        title = models.CharField(max_length=50)
        description = models.CharField(max_length=250)
        incident_date = models.CharField(max_length=50)
        lat = models.FloatField(max_length=25, default=0.00000)
        lng = models.FloatField(max_length=25, default=0.00000)
        room_number = models.CharField(max_length=25)
        floor = models.CharField(max_length=10)
        weather = models.CharField(max_length=50)
        time = models.CharField(max_length=50)

        def __unicode__(self):
            return self.title

class HazardTable(models.Model):

        title = models.CharField(max_length=50)
        description = models.CharField(max_length=250)
        incident_date = models.CharField(max_length=50)
        lat = models.FloatField(max_length=25, default=0.00000)
        lng = models.FloatField(max_length=25, default=0.00000)
        room_number = models.CharField(max_length=25)
        floor = models.CharField(max_length=10)
        weather = models.CharField(max_length=50)
        time = models.CharField(max_length=50)

        def __unicode__(self):
            return self.title

class ImgTable(models.Model):
    block = models.CharField(max_length=50)
    block_level = models.CharField(max_length=50)
    img_url = models.CharField(max_length=500)

    def __unicode__(self):
        return self.block
