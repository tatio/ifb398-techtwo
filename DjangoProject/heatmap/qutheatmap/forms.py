from django import forms
from qutheatmap.models import HazardInside, InjuryInside

class FilterType(ModelForm):
    TYPE_CHOICES = (
        ('0', 'All'),
        ('1', 'Slip'),
        ('2', 'Trip')
    )
    status = forms.ChoiceField(required=True, widget=forms.RadioSelect(attrs={'Type': ''}), choices=TYPE_CHOICES)

    class Meta:
        model = HazardInside

class FilteringForm(forms.Form):
    status = forms.ChoiceField(choices=HazardInside.TYPE_CHOICES, required=True)

    def filter_types(self, hazardinside):
        assert self.is_valid()

        heatmaparray = hazardinside.applied_to.all().order_by('title')

        if self.cleaned_data['status']:
            heatmaparray = heatmaparray.filter(status=self.cleaned_data['status'])

    return heatmaparray
